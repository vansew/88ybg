package com.ybg.base.jdbc;


public class DataBaseConstant {
	public static final String DB_SYS="dataSourceSys";
	public static final String DB_OA="dataSourceOa";
	public static final String DB_EDU="dataSourceEdu";
	public static final String JD_SYS="sysJdbcTemplate";
	public static final String JD_OA="oaJdbcTemplate";
	public static final String JD_EDU="eduJdbcTemplate";
}
